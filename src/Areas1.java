import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Areas1 {
	private String opc;
	private String b;
	private String alt;
	private int _opc;
	private int a;
	private int _b;
	private int _alt;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));//buffer to read from string
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));//bufer write from string
    
	public void init(){

	    try{
	    	System.out.println("opcion 1: triangulo\nopcion 0: salir");
	    	String opc = br.readLine();
	    	Integer _opc = Integer.valueOf(opc);
	    	if(_opc == 1){
	    		System.out.println("Ingrese base:");
	    		String b = br.readLine();
	    		System.out.println("Ingrese altura:");
	    		String alt = br.readLine();
	    		System.out.println("Sus datos:"+b+" "+alt+".\n");
	    		Integer _b = Integer.valueOf(b);
	    		Integer _alt = Integer.valueOf(alt);
	    		a = (_b * _alt)/2;
	    		System.out.println("Respuesta: "+a);
	    	}else if(_opc == 0){

	    	}
	    	bw.flush();
	    }catch (Exception e) {
	    	e.printStackTrace();
		}
	   
	}
	
	public static void main(String[] args) {
		Areas1 demo = new Areas1();
	    long time_start, time_end;
	    time_start = System.currentTimeMillis();
	    demo.init(); // llamamos a la tarea
	    time_end = System.currentTimeMillis();
	    System.out.println("tomo "+ ( time_end - time_start ) +" millisegundos");
	}
}
